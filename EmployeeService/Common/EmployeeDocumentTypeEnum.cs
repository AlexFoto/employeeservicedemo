﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmployeeService.Common
{
    public enum EmployeeDocumentTypeEnum
    {
        Passport,
        License
    }
}
