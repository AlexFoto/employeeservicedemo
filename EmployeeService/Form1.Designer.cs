﻿namespace EmployeeService
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.xpCollection = new DevExpress.Xpo.XPCollection(this.components);
            this.session1 = new DevExpress.Xpo.Session(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFirstName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiddleName2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthDate2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthPlace = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOid = new DevExpress.XtraGrid.Columns.GridColumn();
            this.employee = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFirstName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiddleName1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthDate1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFirstName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colLastName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMiddleName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colBirthDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.button1 = new System.Windows.Forms.Button();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpCollection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.employee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            this.SuspendLayout();
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.xpCollection;
            this.gridControl1.Location = new System.Drawing.Point(12, 41);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(859, 531);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.UseEmbeddedNavigator = true;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // xpCollection
            // 
            this.xpCollection.ObjectType = typeof(EmployeeService.EmployeeWorkCode.Employees);
            this.xpCollection.Session = this.session1;
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colFirstName2,
            this.colLastName2,
            this.colMiddleName2,
            this.colBirthDate2,
            this.colBirthPlace,
            this.colOid});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.True;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            // 
            // colFirstName2
            // 
            this.colFirstName2.FieldName = "FirstName";
            this.colFirstName2.MinWidth = 25;
            this.colFirstName2.Name = "colFirstName2";
            this.colFirstName2.Visible = true;
            this.colFirstName2.VisibleIndex = 0;
            this.colFirstName2.Width = 201;
            // 
            // colLastName2
            // 
            this.colLastName2.FieldName = "LastName";
            this.colLastName2.MinWidth = 25;
            this.colLastName2.Name = "colLastName2";
            this.colLastName2.Visible = true;
            this.colLastName2.VisibleIndex = 1;
            this.colLastName2.Width = 201;
            // 
            // colMiddleName2
            // 
            this.colMiddleName2.FieldName = "MiddleName";
            this.colMiddleName2.MinWidth = 25;
            this.colMiddleName2.Name = "colMiddleName2";
            this.colMiddleName2.Visible = true;
            this.colMiddleName2.VisibleIndex = 2;
            this.colMiddleName2.Width = 135;
            // 
            // colBirthDate2
            // 
            this.colBirthDate2.FieldName = "BirthDate";
            this.colBirthDate2.MinWidth = 25;
            this.colBirthDate2.Name = "colBirthDate2";
            this.colBirthDate2.Visible = true;
            this.colBirthDate2.VisibleIndex = 3;
            this.colBirthDate2.Width = 269;
            // 
            // colBirthPlace
            // 
            this.colBirthPlace.FieldName = "BirthPlace";
            this.colBirthPlace.MinWidth = 25;
            this.colBirthPlace.Name = "colBirthPlace";
            this.colBirthPlace.Visible = true;
            this.colBirthPlace.VisibleIndex = 4;
            this.colBirthPlace.Width = 94;
            // 
            // colOid
            // 
            this.colOid.FieldName = "Oid";
            this.colOid.MinWidth = 25;
            this.colOid.Name = "colOid";
            this.colOid.Width = 94;
            // 
            // employee
            // 
            this.employee.Name = "employee";
            // 
            // colFirstName1
            // 
            this.colFirstName1.Name = "colFirstName1";
            // 
            // colLastName1
            // 
            this.colLastName1.Name = "colLastName1";
            // 
            // colMiddleName1
            // 
            this.colMiddleName1.Name = "colMiddleName1";
            // 
            // colBirthDate1
            // 
            this.colBirthDate1.Name = "colBirthDate1";
            // 
            // colFirstName
            // 
            this.colFirstName.Name = "colFirstName";
            // 
            // colLastName
            // 
            this.colLastName.Name = "colLastName";
            // 
            // colMiddleName
            // 
            this.colMiddleName.Name = "colMiddleName";
            // 
            // colBirthDate
            // 
            this.colBirthDate.Name = "colBirthDate";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.button1);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(883, 584);
            this.layoutControl1.TabIndex = 1;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(883, 584);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 29);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(863, 535);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(859, 25);
            this.button1.TabIndex = 4;
            this.button1.Text = "Update Grid";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.button1;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(863, 29);
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextVisible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(889, 588);
            this.Controls.Add(this.layoutControl1);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.Text = "MainForm";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xpCollection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.session1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.employee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView employee;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstName;
        private DevExpress.XtraGrid.Columns.GridColumn colLastName;
        private DevExpress.XtraGrid.Columns.GridColumn colMiddleName;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthDate;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstName1;
        private DevExpress.XtraGrid.Columns.GridColumn colLastName1;
        private DevExpress.XtraGrid.Columns.GridColumn colMiddleName1;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthDate1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.Xpo.Session session1;
        private DevExpress.XtraGrid.Columns.GridColumn colFirstName2;
        private DevExpress.XtraGrid.Columns.GridColumn colLastName2;
        private DevExpress.XtraGrid.Columns.GridColumn colMiddleName2;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthDate2;
        private DevExpress.Xpo.XPCollection xpCollection;
        private DevExpress.XtraGrid.Columns.GridColumn colBirthPlace;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraGrid.Columns.GridColumn colOid;
        private System.Windows.Forms.Button button1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
    }
}

