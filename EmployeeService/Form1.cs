﻿using System;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid.Views.Grid;
using EmployeeService.Common;
using EmployeeService.EmployeeWorkCode;

namespace EmployeeService
{
    public partial class Form1 : XtraForm
    {
        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            XPBaseObject.AutoSaveOnEndEdit = true;
            if (xpCollection.Count == 0)
            {
                var employees = new Employees(session1);
                employees.FirstName = "John";
                employees.MiddleName = "Second";
                employees.LastName = "King";
                employees.BirthDate = new DateTime(2020, 11, 11);
                employees.BirthPlace = "Ukraine";
                employees.Save();
                var document = new EmployeeDocument(session1);
                document.Employee = employees;
                document.EmployeeDocumentIssued = new DateTime(2020, 11, 11);
                document.EmployeeDocumentIssuePlace = "Some place";
                document.EmployeeDocumentNumber = "1";
                document.EmployeeDocumentSeries = "GF";
                document.EmployeeDocumentType = EmployeeDocumentTypeEnum.Passport;
                document.Save();
                xpCollection.Add(employees);
                var employees2 = new Employees(session1);
                employees2.FirstName = "John";
                employees2.MiddleName = "First";
                employees2.LastName = "King";
                employees2.BirthDate = new DateTime(2020, 11, 11);
                employees2.BirthPlace = "Ukraine";
                employees2.Save();
                xpCollection.Add(employees2);
            }
        }

        private void gridView1_RowCellClick(object sender, RowCellClickEventArgs e)
        {
            var oId = Convert.ToInt32(gridView1.GetRowCellValue(gridView1.FocusedRowHandle, "Oid"));
            var form = new EmployeeCard(oId);
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            session1.DropIdentityMap();
            xpCollection.Reload();
            XPBaseObject.AutoSaveOnEndEdit = true;
        }
    }
}