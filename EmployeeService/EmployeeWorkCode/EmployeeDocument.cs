﻿using System;
using DevExpress.Xpo;
using EmployeeService.Common;

namespace EmployeeService.EmployeeWorkCode
{
    public class EmployeeDocument : XPObject
    {
        private Employees employee;
        private DateTime employeeDocumentIssued;
        private string employeeDocumentIssuePlace;
        private string employeeDocumentNumber;

        private string employeeDocumentSeries;

        private EmployeeDocumentTypeEnum employeeDocumentType;

        public EmployeeDocument()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }


        public EmployeeDocument(Session session) : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        [Association("Employee-EmployeeDocuments")]
        public Employees Employee
        {
            get => employee;
            set => SetPropertyValue(nameof(Employees), ref employee, value);
        }

        [DbType("nvarchar(30)")]
        public EmployeeDocumentTypeEnum EmployeeDocumentType
        {
            get => employeeDocumentType;
            set => SetPropertyValue("EmployeeDocumentType", ref employeeDocumentType, value);
        }

        [DbType("nvarchar(30)")]
        public string EmployeeDocumentSeries
        {
            get => employeeDocumentSeries;
            set => SetPropertyValue<string>("EmployeeDocumentSeries", ref employeeDocumentSeries, value);
        }

        [DbType("nvarchar(30)")]
        public string EmployeeDocumentNumber
        {
            get => employeeDocumentNumber;
            set => SetPropertyValue<string>("EmployeeDocumentNumber ", ref employeeDocumentNumber, value);
        }

        public DateTime EmployeeDocumentIssued
        {
            get => employeeDocumentIssued;
            set => SetPropertyValue<DateTime>("EmployeeDocumentIssued  ", ref employeeDocumentIssued, value);
        }

        [DbType("nvarchar(200)")]
        public string EmployeeDocumentIssuePlace
        {
            get => employeeDocumentIssuePlace;
            set => SetPropertyValue<string>("BirthPlace  ", ref employeeDocumentIssuePlace, value);
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}