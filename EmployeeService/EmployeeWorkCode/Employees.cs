﻿using System;
using System.Drawing;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace EmployeeService.EmployeeWorkCode
{
    public class Employees : XPObject
    {
        private DateTime birthDate;
        private string birthPlace;

        private string firstName;
        private Image image;
        private string lastName;
        private string middleName;

        public Employees()
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public Employees(Session session) : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        [Association("Employee-EmployeeDocuments")]
        public XPCollection<EmployeeDocument> EmployeeDocuments => GetCollection<EmployeeDocument>(nameof(EmployeeDocuments));

        [DbType("nvarchar(50)")]
        public string FirstName
        {
            get => firstName;
            set => SetPropertyValue<string>("FirstName", ref firstName, value);
        }

        [DbType("nvarchar(50)")]
        public string LastName
        {
            get => lastName;
            set => SetPropertyValue<string>("LastName", ref lastName, value);
        }

        [DbType("nvarchar(50)")]
        public string MiddleName
        {
            get => middleName;
            set => SetPropertyValue<string>("MiddleName ", ref middleName, value);
        }

        public DateTime BirthDate
        {
            get => birthDate;
            set => SetPropertyValue<DateTime>("BirthDate  ", ref birthDate, value);
        }

        [DbType("nvarchar(250)")]
        public string BirthPlace
        {
            get => birthPlace;
            set => SetPropertyValue<string>("BirthPlace  ", ref birthPlace, value);
        }

        [ValueConverter(typeof(ImageValueConverter))]
        public Image Image
        {
            get => image;
            set => SetPropertyValue(nameof(Image), ref image, value);
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}