﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using EmployeeService.EmployeeWorkCode;

namespace EmployeeService
{
    public partial class EmployeeCard : XtraForm
    {
        public EmployeeCard(int id)
        {
            InitializeComponent();
            SetData(id);
        }

        private void SetData(int id)
        {
            XPBaseObject.AutoSaveOnEndEdit = false;
            var employees = unitOfWork1.Query<Employees>();
            var employeeDos = unitOfWork1.Query<EmployeeDocument>();
            var emp = employees.Where(x => x.Oid == id).FirstOrDefault();
            var docs = employeeDos.Where(x => x.Employee.Oid == id);
            var filterCriteria = new BinaryOperator(nameof(EmployeeDocument.Employee.Oid), id, BinaryOperatorType.Equal);
            label7.Text = emp.Oid.ToString();
            textEdit1.Text = emp.FirstName;
            textEdit2.Text = emp.LastName;
            textEdit3.Text = emp.MiddleName;
            dateTimePicker1.Value = emp.BirthDate;
            textEdit4.Text = emp.BirthPlace;
            pictureEdit1.Image = emp.Image;
            xpCollection1.Filter = filterCriteria;
        }


        private void cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ok_Click(object sender, EventArgs e)
        {
            using (unitOfWork1)
            {
                try
                {
                    var employees = unitOfWork1.Query<Employees>();
                    var emp = employees.Where(x => x.Oid == Convert.ToInt32(label7.Text)).FirstOrDefault();
                    emp.FirstName = textEdit1.Text;
                    emp.LastName = textEdit2.Text;
                    emp.MiddleName = textEdit3.Text;
                    emp.BirthDate = dateTimePicker1.Value;
                    emp.BirthPlace = textEdit4.Text;
                    emp.Image = pictureEdit1.Image;
                    emp.Save();
                    unitOfWork1.CommitChanges();
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            Close();
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            var dialogWindow = new OpenFileDialog();
            if (dialogWindow.ShowDialog(this) == DialogResult.OK)
            {
                var filePath = dialogWindow.InitialDirectory + dialogWindow.FileName;
                ;
                pictureEdit1.Image = Image.FromFile(filePath);
            }
        }
    }
}